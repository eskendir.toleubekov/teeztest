﻿Storage storage = new Storage();
Orders orders = new Orders();
orders.RandomOrders(100, 50);
double cost = 0;
Courier courier = new Courier();

Console.WriteLine("starting transfers...");
orders.CustomerList.ForEach(customerWithSku =>
{
    var sortedListCartSku = new List<(Point, int quantity)>();
    customerWithSku.Cart.ForEach(sku =>
    {
        var s = storage.Stores.First(x => x.Key == sku.Item1.SKU_ID).Value;
        sortedListCartSku.Add((s, sku.Quantity));
    });
    sortedListCartSku = sortedListCartSku.OrderBy(x => x.Item1.X).ToList();
    for (int i = 0; i < sortedListCartSku.Count; i++)
    {
        if (sortedListCartSku.Count != i + 1)
            cost += courier.Transfer(customerWithSku.Cart, sortedListCartSku[i].Item1, sortedListCartSku[i + 1].Item1);
        else
            cost += courier.Transfer(customerWithSku.Cart, sortedListCartSku[i].Item1, customerWithSku.Address);
    }
});
/*Задание:
* Есть склад - Storage - где SKU (товары) хранятся каждый в своей одной ячейке. Складкой запас неограничен.
* Orders - список клиентов, у каждого из которых есть корзина. В корзине случайные товары в случайном количестве.
* Товары по складу и до клиентов перемещает класс Courier, берущий плату только за расстояние.
* Склад, курьер и клиенты находятся в физическом мире. Заказ считается доставленным, если он перемещен в точку расположения клиента.
* Емкость курьера, сколько товаров помещается в точку, и т.д. нас не ограничивают.
* В готовом коде классов могут быть баги или ошибки - его допускается редактировать.
* Правильность ваших действий ничем не проверяется! Курьер просто переместит то, что ему дали, в заданную точку, и оставит посылку у двери.

Критерии выполнения:
* Все товары перемещены до клиентов в физическом мире
* Курьер должен получить правильный список товаров для перемещения
* Чем меньше затрат, тем лучше
* Вводить дополнительные сущности можно, но стоимость перевозки нельзя игнорировать
* Если решение работает в коде, но не сработает в реальности - объяснить почему*/


// ...
// your code here
// ...

Console.WriteLine($"Total Cost: {cost}");



// end of main code
// you may edit classes (sensibly!) or add new ones

class SKU
{
    public int SKU_ID { get; set; }
}
class Point
{
    public int X { get; set; }
    public int Y { get; set; }
}
class Storage
{
    public Dictionary<int, Point> Stores { get; set; } = new Dictionary<int, Point>();
    public Storage()
    {
        for (int i = 0; i < 100; i++)
        {
            int skuId = i + 1;
            int x = i % 10;
            int y = i / 10;
            Stores[skuId] = new Point { X = x, Y = y };
        }
    }
}
class Customer
{
    public Point? Address { get; set; }
    public List<(SKU, int Quantity)>? Cart { get; set; }
}
class Orders
{
    public List<Customer> CustomerList { get; set; } = new List<Customer>();
    public void RandomOrders(int numberOfCustomers, int maxItemsInCart)
    {
        Random random = new Random();

        for (int i = 0; i < numberOfCustomers; i++)
        {
            Customer customer = new Customer
            {
                Address = new Point { X = random.Next(100, 1000), Y = random.Next(100, 1000) },
                Cart = new List<(SKU, int Quantity)>()
            };
            int cartSize = random.Next(1, maxItemsInCart + 1);
            for (int j = 0; j < cartSize; j++)
            {
                SKU sku = new SKU { SKU_ID = random.Next(1, 101) };
                int quantity = random.Next(1, 11);
                customer.Cart.Add((sku, quantity));
            }
            CustomerList.Add(customer);
        }
    }
}
class Courier
{
    double payment;
    public double Transfer(List<(SKU, int Quantity)> skuList, Point from, Point to)
    {
        // imagine there is an API we call that tells the courier what to transport and where
        // to make the task simpler, this api call is omitted - but the solution must give the courier the correct list
        //
        // HttpResponseMessage response = await client.PostAsJsonAsync("api/courier/pickup", skuList);
        // HttpResponseMessage response = await client.PostAsJsonAsync("api/courier/move", to);
        // 
        payment = Math.Sqrt(Math.Pow(from.X - to.X, 2) + Math.Pow(from.Y - to.Y, 2));
        Console.WriteLine($"Moving {skuList.Count} SKUs from ({from.X}, {from.Y}) to ({to.X}, {to.Y}), cost {payment}. ");
        return payment;
    }

    public decimal ConvertPayment(decimal amount)
    {
        return amount;
    }
}